import { Component, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-child-one',
  templateUrl: './child-one.component.html',
  styleUrls: ['./child-one.component.css']
})
export class ChildOneComponent implements OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  constructor() {
    console.log("Child-One : Constructor");
  }

  ngOnInit(): void {
    console.log("--------Child-One : ngOnInit");
  }

  ngDoCheck(): void {
    console.log("--------Child-One : ngDoCheck");
  }

  ngAfterContentInit():void {
    console.log("--------Child-One : ngAfterContentInit");
  }
  
  ngAfterContentChecked():void {
    console.log("--------Child-One : ngAfterContentChecked");
  }
  
  ngAfterViewInit():void {
    console.log("--------Child-One : ngAfterViewInit");
  }

  ngAfterViewChecked(): void {
    console.log("--------Child-One : ngAfterViewChecked");
  }

  ngOnDestroy():void {
    console.log("--------Child-One: ngOnDestroy");
  }

}
