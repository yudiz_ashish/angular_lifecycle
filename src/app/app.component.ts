import { Component, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {
  title = 'Parent';
  showChild : boolean = true;
  constructor() {
    console.log("Parent : Constructor");
  }

  ngOnInit(): void {
    console.log("Parent : ngOnInit");
    // setTimeout(()=>this.hideChild(),3000);
  }

  ngDoCheck(): void {
    console.log("Parent : ngDoCheck");
  }

  ngAfterContentInit():void {
    console.log("Parent : ngAfterContentInit");
  }
  
  ngAfterContentChecked():void {
    console.log("Parent : ngAfterContentChecked");
  }
  
  ngAfterViewInit():void {
    console.log("Parent : ngAfterViewInit");
  }

  ngAfterViewChecked(): void {
    console.log("Parent : ngAfterViewChecked");
  }

  ngOnDestroy():void {
    console.log("Parent: ngOnDestroy");
  }

  // hideChild(){
  //   this.showChild = false;
  // }
}
